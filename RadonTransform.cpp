﻿// RadonTransform.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//


#include <iostream>
#include "RadonTransform.h"

int main()
{

	Radon rad = Radon();
	rad.ShowOriginalImage();

	//----Need use a power of two---//
	rad.RadonTransform1(128, 128);
	rad.ShowSinogramm();
	rad.Back_RadonTransform1();
	rad.ShowRecoveryImage();
	waitKey(0);
}

