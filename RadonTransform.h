﻿#pragma once
#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp> 
#include <list>
#include <vector>

using namespace std;
using namespace cv;


class Radon
{
public:
	string path = "Lenna.png"; 
	Mat originalImage;
	Mat cloneImage;
	Mat cross_imag;
	Point2f img_c;
	vector<Point> vec_boundary_dots;
	double eps = 0.001;
	double s_min = -1;
	double s_max = 1;
	double tetta_min = 0;
	double tetta_max = 1;
	double tetta_step = 0;
	double s_step = 0;
	int s_dots = 0;
	int tetta_dots = 0;
	Mat radonImage;
	Mat recoveryImage;

	Radon()
	{

		originalImage = imread(path, CV_32FC1);
		cvtColor(originalImage, originalImage, COLOR_BGR2GRAY);
		img_c.x = originalImage.rows / 2;
		img_c.y = originalImage.cols / 2;
		cloneImage = originalImage.mul(0);

	}


	void ShowOriginalImage()
	{
		if (!originalImage.empty())
		{
			namedWindow("imag", WINDOW_AUTOSIZE);
			cv::imshow("imag", originalImage);
		}
	}
	void ShowSinogramm()
	{

		if (!radonImage.empty())
		{
			namedWindow("sinogram", WINDOW_AUTOSIZE);
			imshow("sinogram", radonImage);
		}

	}
	void ShowRecoveryImage()
	{
		if (!recoveryImage.empty())
		{
			namedWindow("Recovery", WINDOW_AUTOSIZE);
			imshow("Recovery", recoveryImage);
		}
	}

	bool CrossDots(double k0, double b0, double k, double b, Point& p)
	{
		if (abs(k - k0) < eps) return false;

		p.x = (b - b0) / (k0 - k);
		p.y = k0 * p.x + b0;
		return true;
	}
	bool CrossDots(double k0, double b0, double z, Point& p)
	{
		if (abs(k0) > (1. / eps))
		{
			p.x = z;
			p.y = b0;
			return false;
		}
		else
		{
			p.x = z;
			p.y = k0 * p.x + b0;
			return true;
		}
	}

	void GetKB(double s, double tetta, double& k, double& b)
	{
		tetta *= -CV_PI;
		s *= sqrt(pow(img_c.x, 2) + pow(img_c.y, 2));
		double alfa = (CV_PI / 2) - tetta;
		k = -tan(alfa);
		b = s * sin(tetta) - k * s * cos(tetta);
	}

	void FillingVecBoundatyDots(double k, double b, double s, Point& boundaryDots)
	{

		if (CrossDots(k, b, img_c.x, boundaryDots)) vec_boundary_dots.push_back(boundaryDots);
		if (CrossDots(k, b, -img_c.x, boundaryDots)) vec_boundary_dots.push_back(boundaryDots);
		if (CrossDots(k, b, 0., img_c.y, boundaryDots))vec_boundary_dots.push_back(boundaryDots);
		if (CrossDots(k, b, 0., -img_c.y, boundaryDots))vec_boundary_dots.push_back(boundaryDots);

		if (abs(k) > (1. / eps))
		{
			boundaryDots.x = s;
			boundaryDots.y = img_c.y;
			vec_boundary_dots.push_back(boundaryDots);
			boundaryDots.x = s;
			boundaryDots.y = -img_c.y;
			vec_boundary_dots.push_back(boundaryDots);
		}
		for (int i = 0; i < vec_boundary_dots.size(); i++)
		{
			vec_boundary_dots[i].x += img_c.x;
			vec_boundary_dots[i].y += img_c.y;
		}
	}

	Scalar CreateLineAndMulImage(double tetta)
	{
		Scalar scalar;
		if (vec_boundary_dots.size() >= 2)
		{
			if (abs(tetta) < eps)
			{
				cv::line(cloneImage, vec_boundary_dots[2], vec_boundary_dots[3], Scalar(1, 1, 1), 1);
			}
			else
			{
				cv::line(cloneImage, vec_boundary_dots[1], vec_boundary_dots[0], Scalar(1, 1, 1), 1);
			}
		}
		cross_imag = originalImage.mul(cloneImage);
		scalar = cv::sum(cross_imag);
		return scalar;
	}

	cv::Mat line(double s, double tetta, cv::Scalar& sc)
	{
		cloneImage = originalImage.mul(0);
		vec_boundary_dots.clear();

		double k, b;
		GetKB(s, tetta, k, b);
		Point boundaryDots;

		FillingVecBoundatyDots(k, b, s, boundaryDots);
		sc = CreateLineAndMulImage(tetta);

		return cloneImage;
	}

	void RadonTransform1(int dots_s_count, int dots_tetta_count)
	{
		radonImage.release();
		s_dots = dots_s_count;
		tetta_dots = dots_tetta_count;
		s_step = (s_max - s_min) / s_dots;
		tetta_step = (tetta_max - tetta_min) / tetta_dots;
		radonImage = Mat::zeros(s_dots, tetta_dots, CV_32FC3);
		for (int i = 0; i < tetta_dots; i++)
		{
			for (int j = 0; j < s_dots; j++)
			{
				double tetta_now = tetta_min + i * tetta_step;
				double s_now = s_min + j * s_step;
				Scalar buf_pixel;
				line(s_now, tetta_now, buf_pixel);
				radonImage.at<Vec3f>(i, j)[0] = (double)(buf_pixel[0]);
				radonImage.at<Vec3f>(i, j)[1] = (double)(buf_pixel[0]);
				radonImage.at<Vec3f>(i, j)[2] = (double)(buf_pixel[0]);
			}
		}
		normalize(radonImage, radonImage, 1.0, 0.0, NORM_MINMAX);
	}

	void RecombLR(Mat& src, Mat& dst)
	{
		Mat left = src(Rect(0, 0, src.cols / 2, src.rows));
		Mat right = src(Rect(src.cols / 2, 0, src.cols / 2, src.rows));
		Mat help_mat = left.clone();
		right.copyTo(left);
		help_mat.copyTo(right);
		cv::hconcat(left, right, dst);
	}

	void RecombHconcatVconcat(Mat& src, Mat& dst)
	{
		Mat left = src(Range::all(), Range(0, src.cols / 2)).clone();
		Mat right = src(Range::all(), Range(src.cols - src.cols / 2, src.cols)).clone();

		cv::flip(left, left, 1);
		left = left(Rect(0, 0, left.cols - 1, left.rows));
		Mat help = right(Rect(0, 0, 1, right.rows));
		cv::hconcat(help, left, left);
		cv::vconcat(left, right, dst);
	}

		void Recomb(Mat& src, Mat& dst)
	{
		int cx = floorf(src.cols / 2);
		int cy = floorf(src.rows / 2);

		Mat ul, ur, dl, dr;
		dst = src.clone();

		ul = src(Range(0, cy), Range(0, cx)).clone();
		ur = src(Range(0, cy), Range(src.cols - cx, src.cols)).clone();

		dl = src(Range(src.rows - cy, src.rows), Range(0, cx)).clone();
		dr = src(Range(src.rows - cy, src.rows), Range(src.cols - cx, src.cols)).clone();

		ul.copyTo(dst(Range(src.rows - cy, src.rows), Range(src.cols - cx, src.cols)));
		ur.copyTo(dst(Range(src.rows - cy, src.rows), Range(0, cx)));
		dl.copyTo(dst(Range(0, cy), Range(src.cols - cx, src.cols)));
		dr.copyTo(dst(Range(0, cy), Range(0, cx)));
	}

	void showFC2Module(Mat imag, string name)
	{
		Mat dst = Mat::zeros(imag.size(), CV_32FC1);

		for (int i = 0; i < imag.rows; i++)
		{
			for (int j = 0; j < imag.cols; j++)
			{
				double real = imag.at<cv::Vec2f>(i, j)[0];
				double image = imag.at<cv::Vec2f>(i, j)[1];

				double data = sqrt(pow(real, 2) + pow(image, 2));
				dst.at<float>(i, j) = sqrt(pow(real, 2) + pow(image, 2));

			}
		}
		dst += Scalar(1);
		cv::log(dst, dst);
		cv::normalize(dst, dst, 0, 1, NORM_MINMAX);
		imshow(name, dst);
	}

	void LinearPolar(Mat& src, Mat& dst)
	{
		Point2f center((float)src.cols / 2, (float)src.rows / 2);
		double maxRadius = 0.7 * min(src.rows, src.cols);
		int flags = INTER_LINEAR + WARP_INVERSE_MAP + WARP_FILL_OUTLIERS;
		cv::linearPolar(src, dst, center, maxRadius, flags);
	}

	void Back_RadonTransform1()
	{
		Mat intermediateImage;

		//------Translation synograms to coordinates (0 - Smax-1)----//
		intermediateImage = Mat::zeros(radonImage.rows, radonImage.cols, CV_32FC2);
		for (int i = 0; i < radonImage.rows; i++)
		{
			for (int j = 0; j < radonImage.cols; j++)
			{
				intermediateImage.at<Vec2f>(i, j)[0] = radonImage.at<cv::Vec3f>(i, (j + radonImage.cols / 2) % radonImage.cols)[0];
			}
		}

		//-------If you want see intermediate step, then comment cv::DFT_SCALE and uncomment ShowFC2Module()-----//
		cv::dft(intermediateImage, intermediateImage, cv::DFT_ROWS | cv::DFT_SCALE | cv::DFT_COMPLEX_OUTPUT);
		//showFC2Module(buffer, "dft");

		//-------Image operation. Permutation image to coordinates X: (0 - Kmax/2-1); Y: (0 - 2*tetta)-------/// 
		RecombLR(intermediateImage, intermediateImage);
		RecombHconcatVconcat(intermediateImage, intermediateImage);
		//showFC2Module(buffer, "Spaced Image");
		
		//-------I don't crop image because when I did this the programm didn't work correctly. I have not found the reason.-----//
		LinearPolar(intermediateImage, intermediateImage);
		//showFC2Module(buffer, "linearPolar");

		cv::idft(intermediateImage, intermediateImage, cv::DFT_COMPLEX_OUTPUT);

		//------I do this permutation here because the image come transformed-----////
		Recomb(intermediateImage, intermediateImage);

		//------Module calculation. I use CV_32FC3 because CV_32FC2 can not be displayed unchanged.----//
		recoveryImage = Mat::zeros(intermediateImage.rows, intermediateImage.cols, CV_32FC3);
		for (int i = 0; i < recoveryImage.rows; i++)
		{
			for (int j = 0; j < recoveryImage.cols; j++)
			{
				double real = intermediateImage.at<cv::Vec2f>(i, j)[0];
				double image = intermediateImage.at<cv::Vec2f>(i, j)[1];

				double data = sqrt(pow(real, 2) + pow(image, 2));
				recoveryImage.at<Vec3f>(i, j)[0] = sqrt(pow(real, 2) + pow(image, 2));
				recoveryImage.at<Vec3f>(i, j)[1] = recoveryImage.at<Vec3f>(i, j)[0];
				recoveryImage.at<Vec3f>(i, j)[2] = recoveryImage.at<Vec3f>(i, j)[0];
			}
		}

		//-------Resize recovery image to original image------//
		cv::resize(recoveryImage, recoveryImage, Size(originalImage.rows, originalImage.cols));
		cv::flip(recoveryImage, recoveryImage, 1);
		
	}

};

